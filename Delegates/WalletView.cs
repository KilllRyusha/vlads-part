using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WalletView : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI coinsText;
    private int _coin;
    private Wallet _coinDebug;
    void Start()
    {
        _coinDebug = GetComponent<Wallet>();
    }

    // Update is called once per frame
    void Update()
    {
        _coin = _coinDebug.Coins;
        coinsText.text = $"Coins: {_coin}";
    }
}
