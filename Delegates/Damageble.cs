﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class Damageble : MonoBehaviour
{
    public static System.Action<float> OnObstacleCollision;
    public static System.Action<int> OnCoinPickUp;
    [SerializeField] private float damage;
    [SerializeField] private int coin;
    [SerializeField] private int bonusMulip;

    void Start()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "LittleCoin")
            OnCoinPickUp?.Invoke(coin);
        if (other.tag == "Coin")
        {
            int _newCoin = coin * bonusMulip;
            OnCoinPickUp?.Invoke(_newCoin);
        }
        if (other.tag == "LittleObstacle")
            OnObstacleCollision?.Invoke(damage);
        if (other.tag == "Obstacle")
        {
            float _newDamage = damage * bonusMulip;
            OnObstacleCollision?.Invoke(_newDamage);
        }
    }

    void Update()
    { 

    }
}
