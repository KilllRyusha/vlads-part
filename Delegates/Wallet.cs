﻿using UnityEngine;


public class Wallet : MonoBehaviour
{
    private int _coins;
    public int Coins => _coins;

    private void OnEnable()
    {
        Damageble.OnCoinPickUp += GetCoins;
    }

    private void OnDisable()
    {
        Damageble.OnCoinPickUp -= GetCoins;
    }

    private void GetCoins(int coin)
    {
        _coins += coin;
    }
}
