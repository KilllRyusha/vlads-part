﻿using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _speed;
    public float Speed => _speed;
    private float _move;
   
    void Start()
    {

    }

    void Update()
    {
       _move= Input.GetAxis("Horizontal") * _speed *Time.deltaTime;
       transform.Translate(_move, 0, 0);



    }
}
