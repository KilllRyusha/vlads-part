using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpView : MonoBehaviour
{
    [SerializeField] private Slider hpSlider;
    private float _health;
    private Health hp;
    void Start()
    {
        hp = GetComponent<Health>();
    }
    void Update()
    {
        _health =hp.HP ;
        hpSlider.value = _health;
    }
}
