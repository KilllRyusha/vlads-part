﻿using UnityEngine;
using UnityEngine.UI;



public class Health : MonoBehaviour
{
    [SerializeField] private float _health;
    public float HP => _health;
    bool isAlive => HP > 0;

    private void OnEnable()
    {
        Damageble.OnObstacleCollision += TakeDamage;
    }
    private void OnDisable()
    {
        Damageble.OnObstacleCollision -= TakeDamage;
    }

    private void TakeDamage(float dmg)
    {
        if (isAlive == true)
        {
            _health -= dmg;
        }
    }
   
    void Start()
    {
    }


    void Update()
    {
    }
}
