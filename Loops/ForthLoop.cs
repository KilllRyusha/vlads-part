﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForthLoop : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 60; i <= 100; i++)
        {
            if (i < 70)
            {
                Debug.Log("За " + i + " баллов вы получили удовлетворительно!");
            }
            else if (i >= 70&&i<80)
            {
                Debug.Log("За " + i + " баллов вы получили хорошо!");
            }
            else if (i >= 80 )
            {
                Debug.Log("За " + i + " баллов вы получили отлично!");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
