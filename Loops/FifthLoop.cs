﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FifthLoop : MonoBehaviour
{
    public int Num;
    private string shortNum;
    void Start()
    {
        int changeNum=Num;
        int lastPart = Num;
        int discard = 10000;
        for (int i = 0; i < 5; i++)
        {
            lastPart = changeNum;
            changeNum = changeNum % discard;
            Num = (lastPart - changeNum) / discard;
            discard /= 10;
            if (i != 2)
            {
                shortNum += Num;
            }
        }
        Debug.Log(shortNum);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
