﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeventhLoop : MonoBehaviour
{
    int check = 40;
    int change;
    void Start()
    {
        while (check < 51)
        {
            change = check;
            while( change > 1)
            {
                if (change % 2 == 0)
                {
                    change /= 2;
                }
                else
                {
                    change *= 3;
                    change += 1;
                    change /= 2;
                } 
            }
            Debug.Log(check + " , " + change);
            check++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
