﻿using UnityEngine;

public class Dlegates : MonoBehaviour
{
    delegate string Del(string del);
    delegate int Nums(int num,int num2);
    delegate void FactNum(int num);
    delegate string StringSum(string str, string str2);
    delegate string Message(string msg);
    void Start()
    {
        Task1();
        Task2();
        Task3();
    }
    private static string Delegates(string del) => del;
    private void Task1()
    {
        Del del = Delegates;
        Debug.Log(del("Делегаты (^._.^)"));
        Del delCool = Delegates;
        Debug.Log(delCool("это круто"));
        Debug.Log(del("Делегаты (^._.^)") + delCool(" это круто"));
    }
    private static int SumNums(int num, int num2) => num + num2;
    private static int DifNums(int num, int num2) => num - num2;
    //private static int 
    private void Task2()
    {
        Nums num = SumNums;
        Debug.Log(num(7, 7));
        Nums num2 = DifNums;
        Debug.Log(num2(77, 12));
        FactNum factNum = delegate(int _factNum)
        { 
            int oldNum = _factNum;
            for (int i = 1; i < oldNum; i++)
            {
                _factNum *= i;
            }
            Debug.Log(_factNum);
        };
        factNum(7);


    }
    private static string SumOfStrings(string str, string str2) => str+str2;
    private static string Msg(string msg) => msg;
    private void Task3()
    {
        StringSum srtSum = SumOfStrings;
        Debug.Log(srtSum("я", " работаю!"));
        Message msg = Msg;
        Debug.Log(msg("Работает"));
    }
    

    void Update()
    {
        
    }
}
