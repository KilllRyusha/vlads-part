﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeventhTask : MonoBehaviour
{
    public float money=1000;
    public float cost=750;
    public float step=10;
    public float lastBuy=750;
    public bool Bought=true;
    public int interval = 100;
    void Start()
    {
        
    }

   void Buy()
    {
  lastBuy = cost;
            money -= cost;
            Bought = true;
            Debug.Log("Купил за " + cost);
    }
    void Sell()
    {

        money += cost;
        Bought = false;
        Debug.Log("Продал за " + cost);
    }
    void Update()
    {
        cost += Random.Range(-1, 2);
        if(cost+ lastBuy/step <= lastBuy && Bought == false)
        {
            Buy();
        }
        if (cost >= lastBuy + cost / step && Bought==true)
        {
            Sell();
        }
        if (Bought == false)
        {
            int i = 1;
            i++;
            if (i % interval==0)
            {
                Buy();
            }
        }
        if (Bought == true)
        {
            int i = 1;
            i++;
            if (i % interval == 0)
            {
                Sell();
            }
        }
    }
}
