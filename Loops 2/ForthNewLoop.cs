﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForthNewLoop : MonoBehaviour
{
    int soldiers = 1000;
    bool canDebug = true;
    void Start()
    {
        for(int i = 0; i < 33; i++)
        {
            if(canDebug==true &&soldiers>0){
                if (i % 2 == 0)
                {
                    if (soldiers > soldiers / 10)
                    {
                        soldiers -= soldiers / 10;
                    }
                    else
                    {
                        Debug.Log("Последний " + i + " этаж где ещё осталось " + soldiers + " солдат");
                        soldiers = 0;
                        canDebug = false;
                    }
                }
                else
                {
                    if (soldiers > 66)
                    {
                        soldiers -= 66;
                    }
                    else
                    {
                        Debug.Log("Последний "+i+" этаж где ещё осталось " + soldiers + " солдат");
                        soldiers = 0;
                        canDebug = false;
                    }
                }
            }else if(canDebug==false)
            {
                Debug.Log("Всех солдат съели на " + i + " этаже");
                canDebug = true;
            }
        }
       // Debug.Log(soldiers);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
