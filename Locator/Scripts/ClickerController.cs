﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickerController : MonoBehaviour
{
    [SerializeField] private Text levelText;
    [SerializeField] private Text scoreText;
    [SerializeField] private Image levelFill;
    [SerializeField] private AudioSource click;

    private float score;
    private int level=1;
    private float scoreForLevelUp=2;
    private void Awake()
    {
        IController soundController = new SoundController();
        Locator.Provide(soundController);
    }
    void Start()
    {
        scoreText.text = "Score: " + score;
        levelText.text = "Level: " + level;
    }
    

        public void Click()
    {
        Locator.GetController().PlaySound(click);
        score++;
    }
    void Update()
    {
        levelFill.fillAmount = score / scoreForLevelUp;
        if(score>=scoreForLevelUp)
        {
            score = 0;
            scoreForLevelUp *= 1.5f;
            level++;
        }
        scoreText.text = "Score: " + score;
        levelText.text = "Level: " + level;
    }
}
