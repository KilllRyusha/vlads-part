﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenuAttribute(menuName = "Some Object")]
public class TestPerceptron : ScriptableObject
{
    [SerializeField] private double input1;
    public double Input1 => input1;
    [SerializeField] private double input2;
    public double Input2 => input2;
    [SerializeField] private double output;
    public double Output => output;

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (input1 > 1 || input1 < -1)
            input1 = 0;
        if (input2 > 1 || input2 < -1)
            input2 = 0;
        if (output > 1 || output < -1)
            output = 0;
    }
#endif
}
