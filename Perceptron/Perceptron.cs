﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Random = System.Random;

public class Perceptron : MonoBehaviour
{
    [SerializeField] private List<TestPerceptron> perceptronTests = new List<TestPerceptron>();

    private double[] weights = { 0, 0 };
    private double bias;
    private string fullPath;
    private char dot = '.';

    private void Awake()
    {
        string pathToFile = "C:";
        string fileName = "data.txt";
        fullPath = Path.Combine(pathToFile, fileName);

        for (int i = 0; i < weights.Length; i++)
        {
            weights[i] = GetRandomDouble(-1, 1);
        }
        bias = GetRandomDouble(-1, 1);
    }

    private void Start()
    {
        if (File.Exists(fullPath))
        {
            Load();
        }
        else
            Learn();
    }

    private void Learn()
    {
        double error = 1;
        do
        {
            error = 0;
            foreach (var item in perceptronTests)
            {
                int output = CalculateOutput(item.Input1, item.Input2, bias, weights);
                double localError = item.Output - output;

                weights[0] = item.Input1 * localError + weights[0]; 
                weights[1] = item.Input2 * localError + weights[1]; 

                bias += localError;

                error += System.Math.Abs(localError);
                
                Debug.Log($"we[0] {weights[0]}, we[1] {weights[1]}, bias {bias} \n outp {output} \n err {error}");

                if (error < 0.5)
                {
                    if (output == 1)
                    {
                        Debug.Log("outp = 1");
                    }
                    else
                    {
                        Debug.Log("outp != 1");
                    }

                }
            }

        } while (error > 0.5);
        Save();
    }

    private int CalculateOutput(double input1, double input2, double bias, double[] weights)
    {
        double result = input1 * weights[0] + input2 * weights[1] + bias;
        Debug.Log($"Result = {input1 * weights[0]} + {input2 * weights[1]} + {bias}.");

        return (result >= 0) ? 1 : 0;
    }

    private double GetRandomDouble(double min, double max)
    {
        Random rnd = new Random();
        return rnd.NextDouble() * (max - min) + min;
    }

    private void Save()
    {
        string data = $"{weights[0]}:{weights[1]}:{bias}";
        File.WriteAllText(fullPath, data);
    }

    private void Load()
    {
        string Weights = File.ReadAllText(fullPath);
        string[] data;
        data = Weights.Split(dot);
        weights[0] = double.Parse(data[0]);
        weights[1] = double.Parse(data[1]);
    }
}
