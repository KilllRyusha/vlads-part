﻿using UnityEngine;

public abstract class Item : ScriptableObject
{
    [SerializeField] private int _id;
    [SerializeField] private string _name;
    [SerializeField] private int _price;
    [SerializeField] private int _amount;
    [SerializeField] private GameObject prefab;

    public string Name => _name;
    public int Price => _price;
    public int Amount => _amount;
    public int Id => _id;

    private void OnValidate()
    {
        if (_price < 0)
            _price = 0;
        if (_amount < 0)
            _amount = 0;
    }
}

[CreateAssetMenu(menuName = "Items/Weapon")]
public class Weapon : Item
{
    [SerializeField] private float _damage;
    public float Damage => _damage;

    public float DealDamage()
    {
        return _damage;
    }
}
[CreateAssetMenu(menuName = "Items/Food")]
public class Food : Item
{
    [SerializeField] private float _saturation;
    public float Saturation => _saturation;

    public float Saturate()
    {
        return _saturation;
    }
}

[CreateAssetMenu(menuName = "Items/Armour")]
public class Armour : Item
{
    [SerializeField] private float _def;
    public float Defence => _def;

    public float DefenceDamage()
    {
        return _def;
    }
}