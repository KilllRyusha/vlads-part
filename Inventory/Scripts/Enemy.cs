﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(menuName = "Entity/Enemy")]
public class Enemy : MonoBehaviour   
{
    [SerializeField] private float _enemyHp;

    public void TakeDamage(float damage)
    {
        _enemyHp -= damage;
    }
}
