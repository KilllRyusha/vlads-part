﻿using UnityEditor;
using Malee.List;

[CustomEditor(typeof(Inventory))]
public class InventoryEditor : Editor
{
    private ReorderableList inventoryList;

    private void OnEnable()
    {
        inventoryList = new ReorderableList(serializedObject.FindProperty("items"));
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        inventoryList.DoLayoutList();

        serializedObject.ApplyModifiedProperties();
    }
}
