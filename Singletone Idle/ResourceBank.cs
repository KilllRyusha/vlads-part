using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum ResType
{
    WOOD,
    HOUSE,
    MONEY,
    WORKER
}

public class ResourceBank : MonoBehaviour
{

    [SerializeField] private float waitSeconds;
    public Dictionary<ResType, int> resDictionary = new Dictionary<ResType, int>();

    public static System.Action<ResType, int> OnResChanged;
    public static ResourceBank resourceBankInstance;
    
    private void Awake()
    {
        if (resourceBankInstance == null)
        {
            resourceBankInstance = FindObjectOfType<ResourceBank>();
            if (resourceBankInstance == null)
            {
                var go = new GameObject("ResourceBank");
                go.AddComponent<ResourceBank>();
                resourceBankInstance = go.GetComponent<ResourceBank>();
            }
        }

        resDictionary.Add(ResType.WORKER, 2);
        resDictionary.Add(ResType.WOOD, 0);
        resDictionary.Add(ResType.HOUSE, 0);
        resDictionary.Add(ResType.MONEY, 0);
    }
    private void Start()
    {
        StartCoroutine(AddWood());
    }
    public void ChangeResource(ResType res, int val)
    {
        resDictionary[res] = val;
        OnResChanged?.Invoke(res, val);
    }
    public int GetResource(ResType res)
    {
        return resDictionary[res];
    }

    IEnumerator AddWood()
    {
        while (true)
        {
            resDictionary[ResType.WOOD] += resDictionary[ResType.WORKER];
            ChangeResource(ResType.WOOD, resDictionary[ResType.WOOD]);
            yield return new WaitForSeconds(waitSeconds);
        }
    }

}
