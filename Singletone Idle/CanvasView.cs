using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CanvasView : MonoBehaviour
{

    [SerializeField] private ResType resourseType;
    [SerializeField] private TextMeshProUGUI resorceText;
    private void OnEnable()
    {

        ResourceBank.OnResChanged += ShowText;
    }
    private void OnDisable()
    {

        ResourceBank.OnResChanged -= ShowText;
    }
    private void ShowText(ResType res, int val)
    {
        if (res == resourseType)
        {
            resorceText.text = $"{ResourceBank.resourceBankInstance.GetResource(res)}";
        }
    }
}
