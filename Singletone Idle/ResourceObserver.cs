using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceObserver : MonoBehaviour
{
    [SerializeField] private ResType resourseType;
     private int[] value=new int[] {0,0,0,2 };//0-wood,1-house,2-money,3-worker
    
    private void OnEnable()
    {
        ResourceBank.OnResChanged += ChangeResources;
    }
    private void OnDisable()
    {
        ResourceBank.OnResChanged -= ChangeResources;
    }

    private void ChangeResources(ResType res, int val)
    {
       
        int resType = (int)res;//resources order in array
        int resource= ResourceBank.resourceBankInstance.GetResource(res);
        if (res == ResType.WOOD)
        {
            value[resType] = resource;
            if (value[resType] >= 10)
            {
                value[resType] -= 10;
                value[resType + 1]++;

                LocalChangeResourse(res, value[resType]);
                LocalChangeResourse(res+1, value[resType + 1]);
            }
        }
        if (res == ResType.HOUSE)
        {
            value[resType] = resource;
            if (value[resType] >= 5)
            {
                int random = Random.Range(1, 6);

                value[resType] -= random;
                value[resType + 1] = ResourceBank.resourceBankInstance.GetResource(ResType.MONEY);
                value[resType + 1] += random * 100;

                LocalChangeResourse(res, value[resType]);
                LocalChangeResourse(res + 1, value[resType + 1]);
            }
        }
        if (res == ResType.MONEY)
        {
            value[resType] = resource;
            if (value[resType] >= 1000)
            {
                int random = Random.Range(1, 5);

                value[resType] -= random*250;
                value[resType + 1] = ResourceBank.resourceBankInstance.GetResource(ResType.WORKER);
                value[resType + 1] += random ;

                LocalChangeResourse(res, value[resType]);
                LocalChangeResourse(res + 1, value[resType + 1]);
            }
        }
        
    } private void LocalChangeResourse(ResType res, int val) 
    {
        ResourceBank.resourceBankInstance.ChangeResource(res, val);
    }
    
}
