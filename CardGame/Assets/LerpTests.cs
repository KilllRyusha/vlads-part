using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpTests : MonoBehaviour
{
    public Transform[] points;
    public Transform cube;
    public float interpolater;
    public bool go;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 interpolatedPosition = Vector3.Lerp(points[0].position, points[1].position, interpolater);
        cube.position = interpolatedPosition;
        if (interpolater >= 0 &&go==true)
        {
            interpolater -= 0.1f*Time.deltaTime;
            if (interpolater <= 0)
            { go =false; }
        }
        else 
        {
            interpolater += 0.1f * Time.deltaTime;
            if (interpolater >= 1 )
            { go = true; }
        }
    }
}
