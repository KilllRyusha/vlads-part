using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class CardView : MonoBehaviour, IPointerClickHandler
{
    public CardInstance cardInstance;
    private CardGame _cardGame;
    private Animator cardAnim;
    [SerializeField] private TMP_Text cardName;
    [SerializeField] private Image cardColor;
    [SerializeField] private Image cardTexture;
    [SerializeField] private int pos;
    private void Awake()
    {
        _cardGame = FindObjectOfType<CardGame>();
        cardAnim = transform.GetComponent<Animator>();
    }
    public void Init(CardInstance cardInstance)
    {
        this.cardInstance = cardInstance;
        cardName.text = cardInstance.CardAsset.CardName;
        cardColor.color = cardInstance.CardAsset.CardColor;
        cardTexture.sprite = Sprite.Create(cardInstance.CardAsset.CardTexture, new Rect(0.0f, 0.0f, cardInstance.CardAsset.CardTexture.width, cardInstance.CardAsset.CardTexture.height), new Vector2(0.5f, 0.5f), 100.0f); 
    }
    public void RotateCard(bool up)
    {
        if (up)
        {
            cardAnim.SetFloat("AnimSpeed", 1);
            cardAnim.Play("CardRevealAnumation");
        }else
        {
            cardAnim.SetFloat("AnimSpeed", -1);
            cardAnim.Play("CardRevealAnumation");
        }
    }
    public void PlayCard(int layoutId)
    {
        if(_cardGame.CurrentPlayer==0)
        cardInstance.LayoutId = layoutId;
    }
    private void Update()
    {
       pos = cardInstance.CardPosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (cardInstance.LayoutId == 0)
        {
            PlayCard(2);
        }else if(cardInstance.LayoutId == 2)
        {
            PlayCard(0);
        }
    }
}
