using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardGame : Singleton<CardGame>
{

	public Dictionary<CardInstance, CardView> cards = new Dictionary<CardInstance, CardView>();
	public List<CardAsset> startDeck = new List<CardAsset>();
    [SerializeField] private GameObject cardPrefab;
    public int HandCapacity;
    public int CurrentPlayer;
    [Header("Player's Layouts")]
    [SerializeField] private int playerDeckLayout;
    [SerializeField] private int playerHandLayout;
    [SerializeField] private int playerDropLayout;
    [Header("Enemy's Layouts")]
    [SerializeField] private int enemyDeckLayout;
    [SerializeField] private int enemyHandLayout;
    [SerializeField] private int enemyDropLayout;
    private void Awake()
    {

        StartGame();
    }
    public void StartGame()
    {
        for (int i = 0; i < startDeck.Count; i++)
        {
            CreateCard(startDeck[i], playerDeckLayout);
            CreateCard(startDeck[i], enemyDeckLayout);
        }
        ShuffleLayout(playerDeckLayout);
        ShuffleLayout(enemyDeckLayout);
        StartTurn();
    }
    public void CreateCardView(CardInstance instance)
    {
        var card = Instantiate(cardPrefab);
        var view = card.GetComponent<CardView>();
        view.Init(instance);
        cards.Add(instance, view);
    }

    public void CreateCard(CardAsset cardAsset, int layoutNumber)
    {
        var cardInstance = new CardInstance 
        {
            CardAsset=cardAsset 
        };
        CreateCardView(cardInstance);
        cardInstance.CardPosition = GetCardsInLayout(layoutNumber).Count;
        cardInstance.MoveToLayout(layoutNumber);
    }
    public List<CardView> GetCardsInLayout(int LayoutId)
    {
        List<CardView> listOfCardsInLayout = new List<CardView>();
        foreach (CardView keyValue in cards.Values)
        {
            if (keyValue.cardInstance.LayoutId== LayoutId)
            {
                listOfCardsInLayout.Add(keyValue);
            }
        }
        return listOfCardsInLayout;
    }

    public void RecalculateLayout(int layoutId)
    {
        List<CardView> listOfCardsInLayout= GetCardsInLayout(layoutId);
        for (int i = 0; i < listOfCardsInLayout.Count; i++)
        {
            //listOfCardsInLayout[i].transform.SetSiblingIndex(listOfCardsInLayout[i].cardInstance.CardPosition);
            listOfCardsInLayout[i].cardInstance.CardPosition= listOfCardsInLayout[i].transform.GetSiblingIndex();
        }
    }

    public void StartTurn()
    {
        int cardsInLayout = GetCardsInLayout(CurrentPlayer).Count;
        for (int j = 0; j < HandCapacity-cardsInLayout ; j++)
        {
            List<CardView> listOfCardsInLayout;
            if (CurrentPlayer == 0)
            {
                listOfCardsInLayout = GetCardsInLayout(playerDeckLayout);
            }
            else
            {
                listOfCardsInLayout = GetCardsInLayout(enemyDeckLayout);
            }
            if (listOfCardsInLayout.Count > 0)
            {
                listOfCardsInLayout[0].cardInstance.MoveToLayout(CurrentPlayer);
            }
            else
                return;
            Debug.Log(GetCardsInLayout(CurrentPlayer).Count);
        }
    }

    public void ShuffleLayout(int layoutId)
    {
        List<CardView> listOfCardsInLayout = GetCardsInLayout(layoutId);
        for (int i = 0; i < listOfCardsInLayout.Count; i++)
        {//������� ������������� �����
           //listOfCardsInLayout= listOfCardsInLayout[i].OrderBy(x => Random.value).ToList(); 
        }
        //RecalculateLayout(layoutId);
    }

    public void EndTurn()
    {

        List<CardView> listOfCardsInLayout = GetCardsInLayout(2);
        for (int i = 0; i < listOfCardsInLayout.Count; i++)
        {
            if(CurrentPlayer==0)
            listOfCardsInLayout[i].cardInstance.MoveToLayout(playerDropLayout);
            else
                listOfCardsInLayout[i].cardInstance.MoveToLayout(enemyDeckLayout);
        }
        if (CurrentPlayer == 0)
            CurrentPlayer = 1;
        else
            CurrentPlayer = 0;
        if (GetCardsInLayout(playerDeckLayout).Count == 0)
            RefillDeck(playerDropLayout, playerDeckLayout);
        else if(GetCardsInLayout(enemyDeckLayout).Count == 0 )
            RefillDeck(enemyDropLayout, enemyDeckLayout);
        StartTurn();

    }
     public void RefillDeck( int moveFromLayoutId,int moveToLayoutId)
    {
        List<CardView> listOfCardsInLayout= GetCardsInLayout(moveFromLayoutId);
        for (int i = 0; i < listOfCardsInLayout.Count; i++)
        {
            listOfCardsInLayout[i].cardInstance.MoveToLayout(moveToLayoutId);
        }
        ShuffleLayout( moveToLayoutId);
    }
}