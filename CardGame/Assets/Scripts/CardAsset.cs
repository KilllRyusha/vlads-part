using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="CardAsset")]
public class CardAsset : ScriptableObject
{
    [Header("Visual")]
    public string CardName;
    public Color CardColor;
    public Texture2D CardTexture;

    [Header("Data"), Space(10)]
    public int Id;

    public BasicCardData CardData;
}
public class BasicCardData
{
    public int Power;
    public int Cost;
}
public class LegendaryCard : BasicCardData
{

}
