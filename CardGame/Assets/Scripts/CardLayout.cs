using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardLayout : MonoBehaviour
{
    public int LayoutIndex;
    public Vector3 Offset;
    [SerializeField] private float localOffset;
    private CardGame cardGame;
    [SerializeField]private bool FaceUp;
    [SerializeField] private Transform LayoutPivot;
    private RectTransform rectTransform;
    public float Scale;
    private float _mult;
    private bool moved;
    private void Start()
    {
        cardGame = FindObjectOfType<CardGame>();
        rectTransform = transform.GetComponent<RectTransform>();
    }
    void Update()
    {
        ChangeLayout();
    }
    private void ChangeLayout()
    {
        foreach (CardView keyValue in cardGame.GetCardsInLayout(LayoutIndex))
        {
            var cardViewRect = keyValue.transform.GetComponent<RectTransform>();
            SetOffset(cardViewRect);
            cardGame.RecalculateLayout(LayoutIndex);
            keyValue.RotateCard(FaceUp);
            keyValue.transform.SetParent(transform);
            if (keyValue.transform.GetSiblingIndex() > 0)
                //MoveCard(new Vector3(transform.GetChild(keyValue.transform.GetSiblingIndex() - 1).position.x + Offset.x, LayoutPivot.position.y, 0), new Vector3(transform.GetChild(keyValue.transform.GetSiblingIndex()).position.x + Offset.x, LayoutPivot.position.y, 0));
               // MoveCard((new Vector3(transform.GetChild(keyValue.transform.GetSiblingIndex() - 1).position.x + Offset.x, LayoutPivot.position.y, 0), new Vector3(transform.GetChild(keyValue.transform.GetSiblingIndex()).position.x + Offset.x, LayoutPivot.position.y, 0));
                //MoveCard(transform.position, new Vector3(transform.GetChild(keyValue.transform.GetSiblingIndex() - 1).position.x + Offset.x, LayoutPivot.position.y, 0));
                cardViewRect.position = new Vector3(transform.GetChild(keyValue.transform.GetSiblingIndex() -1).position.x + Offset.x, LayoutPivot.position.y, 0);//����� �������� LERP!!! ����� �������� LERP!!! ����� �������� LERP!!! ����� �������� LERP!!!����� �������� LERP!!!
            else
                cardViewRect.position = new Vector3(LayoutPivot.position.x + rectTransform.rect.width / (transform.childCount + Scale), LayoutPivot.position.y, 0);
        }
    }

    private void SetOffset(RectTransform cardRect)
    {
        var localXOffset = Mathf.Round((rectTransform.rect.width + cardRect.rect.width) / (transform.childCount * Scale));
        if(localXOffset<localOffset)//160 - ���������� ������
            Offset = new Vector3(localXOffset, 0, 0);
        else
            Offset = new Vector3(localOffset, 0, 0);
    }

    private float Increaser()
    {
        _mult += Time.deltaTime ;
        if (_mult > 1)
            _mult = 0;
        return _mult;
    }
    private void SetCradPositionInLayout()
    {
        if (moved)
        {

        }
    }
    private void MoveCard(Vector3 firstPos, Vector3 secondPos)
    {
        float mult = 0;
        mult += Time.deltaTime * Scale;
        Vector3.Lerp(firstPos, secondPos, mult);
        if (mult > 1)
            mult = 0;
    }
    private IEnumerator Increaser(float mult)
    {
        while (mult < 1)
        {
            yield return new WaitForSeconds(.1f);
            mult += .1f;
        }
    }
}
