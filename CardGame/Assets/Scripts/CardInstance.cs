using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardInstance 
{
    public CardAsset CardAsset;
    public int LayoutId;
    public int CardPosition;
    public void MoveToLayout(int id)
    {
        LayoutId = id;
        //CardPosition = id;
    }
}
