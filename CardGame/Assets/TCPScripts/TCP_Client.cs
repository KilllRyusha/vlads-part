using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
public class TCP_Client : MonoBehaviour
{
    [SerializeField] private string message;
    [SerializeField]
    private int Port = 8888;

    [SerializeField]
    private string ip = "192.168.0.198";

    public Action<string> onMessageRecieved = (v) => { };


    private TcpClient socketConnection;
    private Thread clientReceiveThread;

    public void StartClient()
    {
        clientReceiveThread = new Thread(new ThreadStart(ListenForData));
        clientReceiveThread.IsBackground = true;
        clientReceiveThread.Start();
    }
    private void ListenForData()
    {
        try
        {
            socketConnection = new TcpClient(ip, Port);

            Byte[] bytes = new Byte[1024];
            while (true)
            {
                NetworkStream stream = socketConnection.GetStream();

                int length;
                while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    var incommingData = new byte[length];
                    Array.Copy(bytes, 0, incommingData, 0, length);
                    string serverMessage = Encoding.ASCII.GetString(incommingData);
                    foreach (string cmd in serverMessage.Split('/'))
                    {
                        if (cmd != string.Empty)
                        {
                            Debug.Log(cmd);
                            //MainThreadDispatcher.Instance.InvokeInMainThread(() => onMessageRecieved(cmd));
                        }
                    }
                }

            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }

    public void SendTcpMessage(string clientMessage)
    {
        SendTcpMessage(clientMessage, new List<string>());
        Debug.Log("Sent message!");
    }

    public void SendTcpMessage(string clientMessage, List<string> parameters)
    {
        if (socketConnection == null)
        {
            return;
        }
        try
        {
            string message = clientMessage + ",";
            foreach (string f in parameters)
            {
                message += f + ",";
            }
            message += "/";


            // Get a stream object for writing. 			
            NetworkStream stream = socketConnection.GetStream();
            if (stream.CanWrite)
            {
                // Convert string message to byte array.                 
                byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(message);
                // Write byte array to socketConnection stream.                 
                stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }

    private void OnDestroy()
    {
        clientReceiveThread.Abort();
        clientReceiveThread = null;
        socketConnection?.Close();
        socketConnection = null;
    }
}
