using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class TCP_Server : MonoBehaviour
{
    [SerializeField]
    private int Port = 8888;

    private TcpListener tcpListener;
    private Thread tcpListenerThread;
    private TcpClient connectedTcpClient;
    public Action<string> onMessageRecieved = (v) => { };
    public void StartServer(int port)
    {
        this.Port = port;
        //tcpListenerTask = new Task(() => { ListenForIncommingRequests(); });
        //tcpListenerTask.Start();
        tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
        tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();
        //Task.Run(()=> { ListenForIncommingRequests(); });
        //tcpListenerTask.IsBackground = true;
        //tcpListenerTask.Start();
        Debug.Log("Server started");
    }

    private void ListenForIncommingRequests()
    {
        try
        {
            // Create listener on localhost port 8052. 			
            tcpListener = new TcpListener(IPAddress.Any, Port);
            tcpListener.Start();
            byte[] bytes = new byte[1024];
            while (true)
            {
                using (connectedTcpClient = tcpListener.AcceptTcpClient())
                {
                    // Get a stream object for reading 					
                    using (NetworkStream stream = connectedTcpClient.GetStream())
                    {
                        int length;
                        // Read incomming stream into byte arrary. 						
                        while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            var incommingData = new byte[length];
                            Array.Copy(bytes, 0, incommingData, 0, length);
                            // Convert byte array to string message. 							
                            string clientMessage = Encoding.ASCII.GetString(incommingData);
                            foreach (string cmd in clientMessage.Split('/'))
                            {
                                if (cmd != string.Empty)
                                {
                                    Debug.Log(cmd);
                                    //MainThreadDispatcher.Instance.InvokeInMainThread(() => onMessageRecieved(cmd));
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("SocketException " + socketException.ToString());
        }
    }


    public void SendTcpMessage(string msg)
    {
        SendTcpMessage(msg, new List<string>());
    }

    /// <summary> 	
    /// Send message to client using socket connection. 	
    /// </summary> 	
   
    public void SendTcpMessage(string msg, List<string> parameters)
    {
        if (connectedTcpClient == null)
        {
            return;
        }

        try
        {
            // Get a stream object for writing. 			
            NetworkStream stream = connectedTcpClient.GetStream();
            if (stream.CanWrite)
            {
                string serverMessage = msg + ",";
                foreach (string f in parameters)
                {
                    serverMessage += f + ",";
                }

                serverMessage += "/";

                // Convert string message to byte array.                 
                byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(serverMessage);
                // Write byte array to socketConnection stream.               
                stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }

    private void OnDestroy()
    {
        tcpListenerThread.Abort();
        tcpListenerThread = null;
        connectedTcpClient?.Close();
        connectedTcpClient = null;
        tcpListener.Stop();
        tcpListener = null;
    }
}