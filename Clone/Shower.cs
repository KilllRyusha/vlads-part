using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Clones
{
    public class Shower : MonoBehaviour
    {
        static Student student = new Student("Dan", "Gribanovsky", 9992, "Dev");
        Student student2 = (Student)student.Clone();
        private void Start()
        {
            student2.Group = "Art";
            Debug.Log(student.Group);
            Debug.Log(student2.Group);
        }
    }
}
