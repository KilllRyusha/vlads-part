using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IClonable
{
    IClonable Clone();
}
public class Student : IClonable
{
    public string Name;
    public string SecondName;
    public int Age;
    public string Group;
    public Student(string name, string secondName, int age,string group)
    {
        Name = name;
        SecondName = secondName;
        Age = age;
        Group = group;
    }


    public IClonable Clone()
    {
        return new Student(Name,SecondName,Age,Group);
    }
}