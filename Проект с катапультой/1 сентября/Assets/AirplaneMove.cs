﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirplaneMove : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField]
    private float speed =10;
    [SerializeField]
    private Vector3 vector3;
    void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(Vector3.forward * speed, ForceMode.Impulse);
        rb.AddForce(Vector3.right * speed, ForceMode.Impulse);
        transform.Rotate(vector3, Space.Self);
    }
}
