﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text FirstScore;
    private int scoreFirst;
    public Text SecondScore;
    private int scoreSecond;
    public RockSpawn spawn;
    public GameObject winPanel;
    public Text WhoWins;
    void Start()
    {
        FirstScore.text = "Очки первого игрока : " + scoreFirst;
        SecondScore.text = "Очки второго игрока : " + scoreSecond;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "FirstBlock")
        {
            scoreFirst++;
            FirstScore.text ="Очки первого игрока : "+ scoreFirst ;
        }
        if (other.tag == "FirstRock")
        {
            spawn.SpawnFirstCannon();
            if (spawn.canSpawnf > 2)
            {
                spawn.canSpawnf++;
            }
        }
        if (other.tag == "SecondRock")
        {
            spawn.SpawnSecondCannon();
                if (spawn.canSpawns > 2)
                {
                    spawn.canSpawns++;
                }
        }
        if (other.tag == "SecondBlock")
        {
            scoreSecond++;
            SecondScore.text = "Очки второго игрока : " + scoreSecond ;
        }
    }
   

    // Update is called once per frame
    void Update()
    {
        if(spawn.canSpawnf > 4&& spawn.canSpawns >4)
        {
            winPanel.SetActive(true);
            if (scoreFirst > scoreSecond)
            {
                WhoWins.text = "Победил Превый игрок со счётом: "+"\n" + scoreFirst + " : " + scoreSecond;
            }else if (scoreFirst < scoreSecond)
            {
                WhoWins.text = "Победил Второй игрок со счётом: " + "\n" + scoreSecond + " : " + scoreFirst;
            }
            else
            {
                WhoWins.text = "Победила дружба со счётом: " + "\n" + scoreSecond + " : " + scoreFirst;
            }

        }
    }
}
