﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RockSpawn : MonoBehaviour
{
    [SerializeField] private GameObject Capatult;
    [SerializeField] private GameObject Catapult;
    [SerializeField]
    private GameObject FirstButton;
    [SerializeField]
    private GameObject SecondButton;
    public int canSpawnf = 0;
    public GameObject[] HpFirst;
    public int canSpawns = 0;
    public GameObject[] HpSecond;
   

    void Start()
    {
        SpawnFirstCannon();
        SpawnSecondCannon();
    }
    public void ReStart()
    {
        SceneManager.LoadScene(0);
    }
    public void SpawnFirstCannon()
    {
        
        if (canSpawnf < 3)
        {
            GameObject Cannon = GameObject.Find("Capatult(Clone)");
            Destroy(Cannon);
            Instantiate(Capatult);
            canSpawnf++;
            HpFirst[canSpawnf-1].SetActive(true);
        }
           
    }
    public void SpawnSecondCannon()
    {
        
        if (canSpawns < 3)
        {
            GameObject Cannon = GameObject.Find("Catapult (Clone)");
            Destroy(Cannon);
            Instantiate(Catapult);
            canSpawns++;
            HpSecond[canSpawns - 1].SetActive(true);
        }
           

    }
    // Update is called once per frame power.speed
    void Update()
    {
        //Fill.fillAmount += 0.2f/power.speed;
        if (canSpawnf >3)
        {
            FirstButton.SetActive(false);
        }
        if (canSpawns >3)
        {
            SecondButton.SetActive(false);
        }
        }
}
