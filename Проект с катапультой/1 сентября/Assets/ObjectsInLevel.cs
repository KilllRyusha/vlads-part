﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsInLevel : MonoBehaviour
{
    public bool destroy;
    public GameObject destroyEffect;
    public GameObject Base;
    void Start()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Table") {
            Instantiate(destroyEffect, transform);
            destroy = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (destroy == true)
        { 
            Rigidbody rb= transform.GetComponent<Rigidbody>();
            rb.isKinematic=true;
            transform.position -= new Vector3(0, 0.05f, 0);
          
        }
        if (transform.position.y <= -1)
        {
            Destroy(Base);
        }
    }
}
