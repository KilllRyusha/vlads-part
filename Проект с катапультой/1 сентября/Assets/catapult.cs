﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class catapult : MonoBehaviour
{
    public float stoper;
    private bool canPull=false;
    private bool throwBall = false;
    public float BallImpulse=2;
    private HingeJoint BallJoint;
    private bool showW =false;

    [SerializeField]
    private Image FillPower;
    [SerializeField]
    private Rigidbody BallRb;
    [SerializeField]
    private string charge;
    [SerializeField]
    private string shot;
    public float speed = 0;
    [SerializeField]
    private GameObject Wait;
    [SerializeField]
    private GameObject Push;
    void Start()
    {
        //BallRb = GetComponent<Rigidbody>();
        BallJoint = GetComponent<HingeJoint>();
        Wait.SetActive(true);
        Push.SetActive(false);
        FillPower.fillAmount = 0;
    }
      public void ReStart()
    {
        SceneManager.LoadScene(0);
    }

    

    void Update()
    {
        //Debug.Log(speed);
        if (Input.GetKey(charge)&&transform.rotation.x>-stoper)
        {
            canPull = true;
           
        }
        else
        {
            canPull = false;
        }
        if (canPull == true)
        {
            transform.Rotate(-0.5f, 0, 0);
            speed = 0;
            Wait.SetActive(false);
            FillPower.fillAmount = -transform.rotation.x / 0.7f;
        }
        else if (canPull == false&& transform.rotation.x<0)
        {

            speed += 0.05f;
            transform.Rotate(0.3f+speed, 0, 0);
            throwBall = true;
            
        }
        if(canPull == false && transform.rotation.x < -0.2f)
        {
            showW = true;
        }
       if(showW==true)
        {
            Push.SetActive(true);
        }
        
        if (Input.GetKey(shot)&&throwBall == true)
        {
            // BallJoint.enabled = false;
            Push.SetActive(false);
            BallRb.AddForce(Vector3.forward * BallImpulse*speed, ForceMode.Impulse);
            Debug.Log(speed);
            showW = false;
            throwBall = false;
            Debug.Log("Пау!");
            //speed = 0;
        }
    }
}
