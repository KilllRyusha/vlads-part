using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Strat
{ public class Stratagy : MonoBehaviour
    {
        [SerializeField] private float moveSpeed;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private float repeatTimes;
        private IStrategy Move = new MoveForwardStrategy();
        private IStrategy Rotate = new RotateStrategy();
        private IStrategy Emmit = new EmmitStrategy();
        public bool CanMove;
        public bool CanRotate;
        public bool CanEmmit;

        private void ExecuteMoveAlgorithm()
        {
            Move.Perform(transform,moveSpeed);
        }
        private void ExecuteRotateAlgorithm()
        {
            Rotate.Perform(transform, rotationSpeed);
        }
        private void ExecuteEmmitAlgorithm()
        {
            Emmit.Perform(transform, repeatTimes);
        }
        private void Update()
        {
            if (CanMove)
            {
                ExecuteMoveAlgorithm();
                CanMove = false;
            }
            if (CanRotate)
            {
                ExecuteRotateAlgorithm();
                CanRotate = false;
            }
            if (CanEmmit)
            {
                ExecuteEmmitAlgorithm();
                CanEmmit = false;
            }
        }

    }
    


    public class MoveForwardStrategy : IStrategy
    {
        public void Perform(Transform transform, float parameter)
        {
            transform.Translate(1*parameter*Time.deltaTime, 0, 0);
        }
    }

    public class RotateStrategy : IStrategy
    {
        public void Perform(Transform transform, float parameter)
        {
            transform.Rotate(0, 0, 1 * parameter * Time.deltaTime  );
        }
    }

    
    public class EmmitStrategy : IStrategy
    {
        public void Perform(Transform transform, float parameter)
        {
            ParticleSystem particle = transform.GetComponent<ParticleSystem>();
            particle.Emit((int)parameter);
        }
    }
}
