using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Strat
{
    public class ButtonsBehavior : MonoBehaviour
    {
        public Stratagy stratagy;

        public void Move()
        {
            stratagy.CanMove = true;
        }
        public void Rotate()
        {
            stratagy.CanRotate = true;
        }
        public void Emmit()
        {
            stratagy.CanEmmit = true;
        }
    } 
}
