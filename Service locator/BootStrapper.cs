﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BootStrapper
{
    [RuntimeInitializeOnLoadMethod]
    public void Init()
    {
        Locator.Init();

        Locator.locatorInstance.RegisterController<IController>(new SoundController());

        SceneManager.LoadSceneAsync(0);
    }
}
