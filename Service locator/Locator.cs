﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Locator
{
    
    private Locator() { }
    private readonly Dictionary<string, IController> controllers = new Dictionary<string, IController>();
    public static Locator locatorInstance { get; private set; }

    public static void Init()
    {
        locatorInstance = new Locator();
    }

    public T Get<T>() where T: IController
    {
        string key = typeof(T).Name;
        if (!controllers.ContainsKey(key))
        {
            Debug.LogError($"{key} not registrated");
            throw new InvalidOperationException();
        }
        return (T)controllers[key];
    }

    public void RegisterController<T>(T controller) where T : IController
    {
        string key = typeof(T).Name;
        if (controllers.ContainsKey(key))
        {
            Debug.LogError($"tried to register{key}");
            return;
        }
        controllers.Add(key, controller);
    }
    
    public void UnRegisterController<T>() where T : IController
    {
        string key = typeof(T).Name;
        if (!controllers.ContainsKey(key))
        {
            Debug.LogError($"{key} is null");
            return;
        }
        controllers.Remove(key);
    }

    public void PlaySound()
    {
        
    }
}
