﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickerController : MonoBehaviour
{
    [SerializeField] private Text levelText;
    [SerializeField] private Text scoreText;
    [SerializeField] private Image levelFill;

    private float score;
    private int level=1;
    private float scoreForLevelUp=2;
    void Start()
    {
        scoreText.text = "Score: " + score;
        levelText.text = "Level: " + level;
    }
    

        public void Click()
    {
        score++;
        //Locator.PlaySound();
    }
    void Update()
    {
        levelFill.fillAmount = score / scoreForLevelUp;
        if(score>=scoreForLevelUp)
        {
            score = 0;
            scoreForLevelUp *= 1.5f;
            level++;
        }
        scoreText.text = "Score: " + score;
        levelText.text = "Level: " + level;
    }
}
