﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinMaxFloat : ObservableFloat
{
    private float minValue;
    private float maxValue;

    private float currValue
    {
        get => Value;
        set
        {
            if (currValue < minValue)
                currValue = minValue;
            if (currValue > maxValue)
                currValue = maxValue;
        }
    }

    public MinMaxFloat(float value, float min, float max) : base(value)
    {
        this.currValue = value;
        this.minValue = min;
        this.maxValue = max;
    }
}
