﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObservableFloat : MonoBehaviour
{
    private float _value { get => _value; set { OnValueChanged?.Invoke(Value); } }
    public float Value => _value;

    public delegate void ValueChangeHandler(float v);
    public event ValueChangeHandler OnValueChanged;

    public ObservableFloat(float _value) { this._value = _value; }
}
