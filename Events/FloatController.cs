﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatController : MonoBehaviour
{
    [SerializeField] private float minF;
    [SerializeField] private float maxF;
    private ObservableFloat var;

    private ValueVisual vv;

    void Awake()
    {
        MinMaxFloat mmf = new MinMaxFloat(var.Value, minF, maxF);
    }

    void Update()
    {
        vv.Init(new MinMaxFloat(var.Value, minF, maxF));
    }
}
