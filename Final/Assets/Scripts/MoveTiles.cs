﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTiles : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float turnOffPosition;
    public ScoreSystem scoreToSpeedUp;
    [SerializeField] private AudioSource click;
    void Start()
    {
        IController soundController = new SoundController();
        Locator.Provide(soundController);
    }

    
    void Update()
    {
        if (Time.timeScale > 0)
        {
            float Speed = (float)scoreToSpeedUp._score/200+moveSpeed;
            transform.Translate(Vector3.left *Speed);
            if (transform.position.x <= turnOffPosition)
            {
                gameObject.SetActive(false);
            }
            
        }
        else
        {Locator.GetController().PlaySound(click);

        }
    }
}
