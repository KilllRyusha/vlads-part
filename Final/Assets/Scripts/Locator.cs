﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

static public class Locator
{
    static IController controller;
    static SoundController soundController;

    public static void Init()
    {
        controller = soundController;
    }

    public static void Provide(IController newController)
    {
        controller = newController;
    }

    public static IController GetController()
    {
        return controller;
    }
}
