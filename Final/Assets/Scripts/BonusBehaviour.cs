﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusBehaviour : MonoBehaviour
{
    public ScoreSystem _score;
    private GameObject Player;
    void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
      _score = Player.GetComponent<ScoreSystem>();
    }
    void Update()
    {
        
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        _score.Load();
        if (collision.tag == "Player")
        {
            _score.UpScore();
            gameObject.SetActive(false);
        }
    }
}
