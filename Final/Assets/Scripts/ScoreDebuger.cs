﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreDebuger : MonoBehaviour
{
    [SerializeField] private TMP_Text[] scoreTxt;
    public ScoreSystem score;
    void Start()
    {
        score._score = 0;
        score.Save();
    }
    
    
    void Update()
    {
        for (int i = 0; i < scoreTxt.Length; i++)
        {
            scoreTxt[i].text = "Score: " + score._score;
        }
    }
}
