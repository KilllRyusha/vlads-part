﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] Tiles;
    [SerializeField] private GameObject[] Bonuces;
    [SerializeField] private Transform SpawnPoint;
    [SerializeField] private float CheckPoint;
    private int TileNumber;
    void Start()
    {
        for(int i = 0; i < Tiles.Length; i++)
        {
            Tiles[i].SetActive(false);
        }
        Tiles[0].SetActive(true);
    }
    void Update()
    {
        if (Tiles[TileNumber].transform.position.x <= CheckPoint)
        {
            TileNumber++;
            if (TileNumber >= Tiles.Length)
        {
            TileNumber = 0;
        }
            Tiles[TileNumber].transform.position = SpawnPoint.position;
            Tiles[TileNumber].SetActive(true);
            for(int i = 1*(TileNumber+1); i < Bonuces.Length; i++)
            {
                Bonuces[i-1].SetActive(true);
            }
        }
        
    }
}
