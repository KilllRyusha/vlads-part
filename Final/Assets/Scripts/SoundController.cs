﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : IController
{
    public void PlaySound(AudioSource playSound)
    {
        playSound.Play();
    }
}
