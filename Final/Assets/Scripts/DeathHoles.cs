﻿using UnityEngine;

public class DeathHoles : MonoBehaviour
{
    [SerializeField] private GameObject DeathWindow;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Hole")
        {
            DeathWindow.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
