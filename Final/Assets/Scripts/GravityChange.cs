﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityChange : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private int gravityPower;
    void Start()
    {
        rb = transform.GetComponent<Rigidbody2D>();  
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)&&rb.gravityScale== gravityPower)
        {
            rb.gravityScale = -gravityPower;
        } 
        else if (Input.GetMouseButtonDown(0))
        {
            rb.gravityScale = gravityPower;
        }
    }
}
