﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ScoreSystem : MonoBehaviour
{
    private string pathToFile;
    private string fileName = "Data.txt";
    private string fullPath;
    string score;
    public double _score;
    private void Awake()
    {
        pathToFile = "C:";
        fullPath = Path.Combine(pathToFile, fileName);
    }
        void Start()
    {
        
    }
    public void Load()
    {
        score = File.ReadAllText(fullPath);
    }
    public void UpScore()
    {
        _score = double.Parse(score);
         _score++;
        Save();
    }
        public void Save()
    {
        File.WriteAllText(fullPath, _score.ToString());
    }
}
