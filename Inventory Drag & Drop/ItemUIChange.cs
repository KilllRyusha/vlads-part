﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(CanvasGroup))]
public class ItemUIChange : MonoBehaviour,IPointerDownHandler,IPointerEnterHandler,IPointerExitHandler, IDragHandler, IEndDragHandler,IBeginDragHandler
{
    private RectTransform rectTransform;
    private static RectTransform dragFrom;
    private static Item draggingItem;
    private CanvasGroup canvasGroup;

    [SerializeField] private AudioSource AudioSource;
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        AudioSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        rectTransform.localScale*=1.5f;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        rectTransform.localScale =new Vector3( 1f,1f,1f);
    }
    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.position = Input.mousePosition;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
    }

    //public void OnBeginDrag(PointerEventData eventData)
    //{
        
    //    Debug.Log();
    //}
    //private void Drop(RectTransform transform)
    //{
    //    draggingItem.SetParent = transform;
    //}
}
