﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class InventorySlot : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler,IDropHandler
{
    private Image Slot;


    void Start()
    {
        Slot = GetComponent<Image>();
    }
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Slot.color = Color.cyan;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Slot.color = Color.white;
    }


}
