using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace Debugs
{ public abstract class CustomDebugerDecorator : IDebugable
    {
        private IDebugable _debugable;
        public CustomDebugerDecorator(IDebugable debugable)
        {
            this._debugable = debugable;
        }
        public void Show(string s)
        {
            if (this._debugable != null)
            {
                this._debugable.Show(s);
            }
        }
    }
    
    public class CustomDebuger : MonoBehaviour
    {
        [SerializeField] private string str;
        private void Awake()
        {
            IDebugable simple = new Debuger();
            DebugerA _debugerA = new DebugerA(simple);
            DebugerB _debugerB = new DebugerB(_debugerA);
            _debugerA.Show(str);
            _debugerB.Show(str);
        }
    }

    public class Debuger : IDebugable
    {
        public void Show(string s)
        {
            Debug.Log(s);
        }
    }
    public class DebugerA : CustomDebugerDecorator
    {
        public DebugerA(IDebugable debugable) : base(debugable)
        {
        }
        public void Show(string s)
        {
            base.Show(s);
            Debug.Log(s.Length);
        }
    }
    public class DebugerB : CustomDebugerDecorator
    {
        public DebugerB(IDebugable debugable) : base(debugable)
        {
        }
        public void Show(string s)
        {
            base.Show(s);
            File.WriteAllText(@"C:\Games\Decorator.txt", s);
        }
    }

}
