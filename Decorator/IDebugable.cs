using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDebugable {
    public void Show(string s);
}
