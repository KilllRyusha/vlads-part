﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondOOP : MonoBehaviour
{
    NPC grey = new NPC();
    NPC orange = new NPC();
    void Start()
    {
        grey.charisma = "exist";
        orange.charisma = "exist";
        orange.intelligence = "exist";
        orange.stamina = "exist";
        grey.Write("grey");
        orange.Write("orange");
        grey.intelligence = "overpowered";
        orange.stamina = "superhero's";
        grey.Write("grey");
        orange.Write("orange");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
public class NPC {
    public string charisma;
    public string intelligence;
    public string stamina;
    public void Write(string npc)
    {
        Debug.Log(npc);
        if (charisma != null)
        {
            Debug.Log("Харизма: "+charisma);
        }
        if (intelligence != null)
        {
            Debug.Log("Интеллект: " + intelligence);
        }
        if (stamina != null)
        {
            Debug.Log("Выносливость: " + stamina);
        }
    }
}

