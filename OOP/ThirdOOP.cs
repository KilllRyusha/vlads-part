﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdOOP : MonoBehaviour
{
    //[SerializeField]
    private Fighter[] fighters = new Fighter[3];
    private List<int> leavers = new List<int>();
    int whoIsAlive;
    private int dead;//(dead)
    int randHit;
    int randTake;


    void Start()
    {
        fighters[0] = new Fighter(3, 20000);
        fighters[1] = new Fighter(4000, 50);
        fighters[2] = new Fighter(5000, 40);
    }

    
    void Update()
    {
        if (dead < 2)
        { dead = 0;
            randHit = Random.Range(0, 3);//attacker
            randTake = Random.Range(0, 3);//taker
            while (randTake == randHit)
            {
                randTake = Random.Range(0, 3);
                Debug.Log("Take" + randTake);
                Debug.Log("Hit" + randHit);
            }

            fighters[randHit].Damage(fighters[randTake]);
            if (fighters[randTake].HP > 0)
            {
                Debug.Log($"Fighter {randHit} deal {fighters[randHit].damage} dmg to Fighter {randTake} HP left: " + fighters[randTake].HP);
            }
            else if (fighters[randTake].HP < 0)
            {
                fighters[randTake].damage = 0;
                for (int notalive = 0; notalive < 3; notalive++)
                {
                    if (fighters[notalive].HP <= 0)
                    {
                        dead++;
                    }
                    else
                        whoIsAlive = notalive;

                }

            }
        }
        else
        {
            Debug.Log($"Fighter {whoIsAlive} wins");
            for (int i = 0; i < fighters.Length; i++)
            {
                Debug.Log($"Fighter{i} has {fighters[i].HP} hp");
            }
        }
    }
}
public class Fighter
{
    public int damage;
    private int hp;

    public Fighter(int dam,int h)
    {
        damage = dam;
        hp = h;
    }

    public int HP
    {
        get
        {
            return hp;
        }
    }
    public void Damage(Fighter man)
    {
        man.hp -= damage;
    }
}
