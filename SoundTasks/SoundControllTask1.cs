﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundControllTask1 : MonoBehaviour
{

    public AudioSource click;
    public AudioSource music;
    public Text pauseText;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Click()
    {
        click.Play();
    }

    public void Pause()
    {
        if (music.isPlaying)
        {
            music.Pause();
            pauseText.text = "Play";
        }
        else
        {
            music.Play();
            pauseText.text = "Pause";
        }
    }
}
