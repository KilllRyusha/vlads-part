﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControllerTask3 : MonoBehaviour
{
    public AudioClip[] clip;
    private AudioSource source;
    private int songNum;

    private float lastPi;
    private float newPi;
    void Start()
    {
        
        source = GetComponent<AudioSource>();
        source.clip = clip[0];
        lastPi = source.pitch;
    }
    public void NextSong()
    {
        if (songNum < 2)
        {
            songNum++;
            source.clip = clip[songNum];
            
        }
        else
        {
            songNum=0;
            source.clip = clip[0];
            
        }
    }
    public void PreviousSong()
    {
        if (songNum > 0)
        {
            songNum--;
            source.clip = clip[songNum];
            

        }
        else
        {
            songNum=2;
            source.clip = clip[2];
            
        }
    }
    public void ChangePitch()
    {
        newPi = lastPi + Random.Range(-0.1f, 0.11f);
        source.pitch = newPi;
    }
    // Update is called once per frame
    void Update()
    {
        Debug.Log(songNum);
    }
}
