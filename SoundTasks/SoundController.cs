﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundController : MonoBehaviour
{
    public AudioSource Snd;

    private Slider slider;
    private AudioClip clip;

    void Start()
    {
        slider = GetComponent<Slider>();
        clip = Snd.clip;
    }
    public void onSliderChange()
    {
        if (Snd.time < clip.length)
        {
            Snd.time = slider.value * clip.length;
        }
    }

    void Update()
    {
        slider.value = Snd.time / clip.length;
    }

    
}
