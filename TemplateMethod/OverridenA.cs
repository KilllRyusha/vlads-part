using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Tests
{

    public class OverridenA :  DoMafth
    {
        public override int AbstractA(string s)
        {
            return s.Length;
        }

        public override int AbstractB()
        {
            return 42;
        }
    }
    public class OverridenB : DoMafth
    {
        int amountOfVowels;
        public override int AbstractA(string s)
        {
            for(int i = 0; i < s.Length; i++) {
                if (s.Contains("a") || s.Contains("e") || s.Contains("i") || s.Contains("o") || s.Contains("u") || s.Contains("y"))
                    amountOfVowels++;
            }
            return amountOfVowels;
        }

        public override int AbstractB()
        {
            return Random.Range(1, 3);
        }
        public override int VirtualA(int a, int b)
        {
            return a*b;
        }
        public override int VirtualB(int c)
        {
            return c%7;
        }
    }
    public abstract class DoMafth
    {
        public abstract int AbstractA(string s);
        public abstract int AbstractB();
        public virtual int VirtualA(int a, int b)
        {
            return a + b;
        }
        public virtual int VirtualB(int c)
        {
            return (int)Mathf.Pow(c,2);
        }
        public int TemplateMethod(string s, int a, int b, int c)
        {
            Debug.Log(AbstractA(s));
                Debug.Log(AbstractB());
                    Debug.Log(VirtualA(a, b));
            Debug.Log(VirtualB(c));
            return ((int)Mathf.Pow(AbstractA(s),AbstractB()) + VirtualA(a, b) * VirtualB(c));

        }
    }
}
