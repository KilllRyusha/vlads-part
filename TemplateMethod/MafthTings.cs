using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Tests
{
    public class MafthTings : MonoBehaviour
    {
        [SerializeField] private string s;
        [SerializeField] private int a;
        [SerializeField] private int b;
        [SerializeField] private int c;
        private OverridenA overridedA= new OverridenA();
        private OverridenB overridenB = new OverridenB();
        void Start()
        {
            Debug.Log(overridedA.TemplateMethod(s,a,b,c));
            Debug.Log(overridenB.TemplateMethod(s, a, b, c));
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
