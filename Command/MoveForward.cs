using UnityEngine;

public class MoveForward : ICommand
{
    private Transform t;
    private float dist;
    public MoveForward(Transform t,float dist)
    {
        this.t = t;
        this.dist = dist;
    }
    public void Invoke()
    {
        t.position = new Vector3(t.position.x + dist, 0, 0);
    }

    public void Undo()
    {
        t.position = new Vector3(t.position.x - dist, 0, 0);
    }
}
