using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invoker : MonoBehaviour
{
    private List<ICommand> _commandsToDo;
    private List<ICommand> _doneCommands;

    public Invoker()
    {
        _commandsToDo = new List<ICommand>();
        _doneCommands = new List<ICommand>();
    }
   public void AddCommand(ICommand command)
    {
        if (command != null)
        {
            _commandsToDo.Add(command);
        }
    }
    public void ProcessAll()
    {
        for (int i = 0; i < _commandsToDo.Count; i++)
        {
            _commandsToDo[i].Invoke();
            _doneCommands.Add(_commandsToDo[i]);
        }
        ClearQueue();
    }
    public void Process()
    {
        if (_commandsToDo.Count > 0)
        {
            _commandsToDo[0].Invoke();
            _doneCommands.Add(_commandsToDo[0]);
            _commandsToDo.RemoveAt(0);
        }
    }
    public void ClearQueue()
    {
        _commandsToDo.Clear();
    }
    public void Undo()
    {
        if (_doneCommands.Count > 0)
        {
            int index = _doneCommands.Count - 1;
            _doneCommands[index].Undo();
            _doneCommands.RemoveAt(index);
        }
    }
}
