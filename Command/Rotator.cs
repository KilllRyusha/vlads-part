using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator :ICommand
{

    private Transform t;
    private float angle;
    public Rotator(Transform t, float angle)
    {
        this.t = t;
        this.angle = angle;
    }
    public void Invoke()
    {
        t.Rotate(0, 0,angle);
    }

    public void Undo()
    {
        t.Rotate(0, 0,-angle);
    }
}
