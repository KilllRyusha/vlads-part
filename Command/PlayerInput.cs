using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private Invoker invoker;
    [SerializeField] private float force=1;
    [SerializeField] private float rot = 45;

    private void OnEnable()
    {
        invoker = new Invoker();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            var command = new MoveForward(transform, force);
            invoker.AddCommand(command);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            var command = new Rotator(transform,rot);
            invoker.AddCommand(command);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            var command = new Rotator(transform, -rot);
            invoker.AddCommand(command);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            invoker.ProcessAll();
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            invoker.Undo();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            invoker.Process();
        }
    }
}
