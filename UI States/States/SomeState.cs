﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomeState : BaseState
{
    private System.Action OnTimeChanged;
    public override void PrepareState()
    {
        owner.Root.SomeView.ShowState();
        owner.Root.SomeView.OnButtonClick += ChangeNext;
        owner.Root.SomeView.OnPreviousButtonClick += ChangeBack;
    }
    public override void UpdateState()
    {
        OnTimeChanged?.Invoke();
    }

    public override void DestroyState()
    {
        owner.Root.SomeView.HideState();
        owner.Root.SomeView.OnButtonClick -= ChangeNext;
        owner.Root.SomeView.OnPreviousButtonClick -= ChangeBack;
    }
    private void ChangeNext()
    {
        owner.ChangeState(new ThirdState());
    }
    private void ChangeBack()
    {
        owner.ChangeState(new MainMenuState());
    }
}
