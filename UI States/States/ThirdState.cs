using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdState : BaseState
{
    public override void PrepareState()
    {
        owner.Root.ThirdView.ShowState();
        owner.Root.ThirdView.OnButtonClick += ChangeNext;
        owner.Root.ThirdView.OnPreviousButtonClick += ChangeBack;
    }

    public override void DestroyState()
    {
        owner.Root.ThirdView.HideState();
        owner.Root.ThirdView.OnButtonClick -= ChangeNext;
        owner.Root.ThirdView.OnPreviousButtonClick -= ChangeBack;
    }

    private void ChangeNext()
    {
        owner.ChangeState(new MainMenuState());
    }
    private void ChangeBack()
    {
        owner.ChangeState(new SomeState());
    }
}
