﻿public class MainMenuState : BaseState
{
    public override void PrepareState()
    {
        owner.Root.MainMenuView.ShowState();
        owner.Root.MainMenuView.OnButtonClick += ChangeNext;
        owner.Root.MainMenuView.OnPreviousButtonClick += ChangeBack;
    }

    public override void DestroyState()
    {
        owner.Root.MainMenuView.HideState();
        owner.Root.MainMenuView.OnButtonClick -= ChangeNext;
        owner.Root.MainMenuView.OnPreviousButtonClick -= ChangeBack;
    }

    private void ChangeNext() 
    {
        owner.ChangeState(new SomeState());
        
    }
    private void ChangeBack()
    {
        owner.ChangeState(new ThirdState());
    }
}
