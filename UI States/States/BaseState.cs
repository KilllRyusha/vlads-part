﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseState 
{
    public UiStateMachine owner;
    public abstract void PrepareState();
    public virtual void UpdateState() { }
    public abstract void DestroyState();
}
