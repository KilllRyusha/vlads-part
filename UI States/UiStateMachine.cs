﻿using UnityEngine;

    public class UiStateMachine : MonoBehaviour
    {
        private BaseState _currentState;

        [SerializeField] private UiRoot root;
        public UiRoot Root => root;

        private void Awake()
        {
            ChangeState(new MainMenuState());
        }

        private void Update()
        {
            if (_currentState != null)
            {
                _currentState.UpdateState();
            }
            
        }

        public void ChangeState(BaseState newState)
        {
            if (_currentState != null)
            {
                _currentState.DestroyState();
            }

            _currentState = newState;

            if (_currentState != null)
            {
                _currentState.owner = this;
                _currentState.PrepareState();
            }
        }
    }
