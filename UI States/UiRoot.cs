﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiRoot : MonoBehaviour
{
    [SerializeField] private MainMenuView mainMenuView;
    public MainMenuView MainMenuView => mainMenuView;
    [SerializeField] private SomeView someView;
    public SomeView SomeView => someView;
    [SerializeField] private ThirdView thirdview;
    public ThirdView ThirdView => thirdview;
}
