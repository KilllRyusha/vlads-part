﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SomeView : BaseView
{
    [Header("First Task")]
    [SerializeField] private Button button;
    [SerializeField] private Button backButton;
    [Space(20)]
    [Header("Second Task")]
    [SerializeField] private List<int> code = new List<int>();
    [SerializeField] private int[] trueCode;
    [SerializeField] private Text text;
    [SerializeField] private Button CodeOneButton;
    [SerializeField] private Button CodeTwoButton;
    [SerializeField] private Button CodeThreeButton;
    private bool _blowUped;
    public bool BlowUped => _blowUped;

    [SerializeField] private float time;
    public float Timer => time;

    private SomeState stateMachine = new SomeState();

    public System.Action OnButtonClick;
    public System.Action OnPreviousButtonClick;
    public System.Action OnTextChange;

     
    public override void ShowState()
    {
            ChangeText();
        base.ShowState();
        button.onClick.AddListener(OnButtonClicked);
        backButton.onClick.AddListener(OnPreviousButtonClicked);
    }
    

    public override void HideState()
    {
        base.HideState();
        button.onClick.RemoveAllListeners();
        backButton.onClick.RemoveAllListeners();
    }

    public void ChangeText()
    {
        text.text = $"Time: {time}";
        //Debug.Log(stateMachine.Timer);
    }

    public void CodeButtonPressed(int num)
    {
        code.Add(num);
        if (code.Count == 3)
        {
            for (int i = 0; i < code.Count; i++)
            {
                if (code[i] != trueCode[i])
                    _blowUped = true;
                Debug.Log(_blowUped);
            }
            OnButtonClicked();
        }
    }
    public void OnButtonClicked()
    {
        OnButtonClick?.Invoke();
    }
        public void OnPreviousButtonClicked()
        {
            OnPreviousButtonClick?.Invoke();
        }
    }

