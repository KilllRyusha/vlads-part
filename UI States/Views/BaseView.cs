﻿using UnityEngine;

public class BaseView : MonoBehaviour
{
    public virtual void ShowState()
    {
        gameObject.SetActive(true);
    }
    public virtual void HideState() 
    {
        gameObject.SetActive(false);
    }
}
