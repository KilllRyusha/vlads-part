﻿using UnityEngine;
using UnityEngine.UI;
    public class MainMenuView : BaseView
    {[Header("First Task")]
        [SerializeField] private Button button;
    [SerializeField] private Button backButton;
    [Space(20)]
    [Header("Second Task")]
    [SerializeField] private Button startButton;

    public System.Action OnButtonClick;
    public System.Action OnPreviousButtonClick;

        public override void ShowState()
        {
            base.ShowState();
            button.onClick.AddListener(OnButtonClicked);
        backButton.onClick.AddListener(OnPreviousButtonClicked);
        startButton.onClick.AddListener(OnButtonClicked);
    }

        public override void HideState()
        {
            base.HideState();
            button.onClick.RemoveAllListeners();
        backButton.onClick.RemoveAllListeners();
        startButton.onClick.RemoveAllListeners();
        }

        public void OnButtonClicked()
        {
            OnButtonClick?.Invoke();
        }
    public void OnPreviousButtonClicked()
    {
        OnPreviousButtonClick?.Invoke();
    }
    }

