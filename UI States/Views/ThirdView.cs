using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThirdView : BaseView
{

    [SerializeField] private Button button;
    [SerializeField] private Button backButton;


    private SomeView blow = new SomeView();
    [SerializeField] private Text Blow;

    public System.Action OnButtonClick;
    public System.Action OnPreviousButtonClick;

    public override void ShowState()
    {
        base.ShowState();
        button.onClick.AddListener(OnButtonClicked);
        backButton.onClick.AddListener(OnPreviousButtonClicked);

        if (!blow.BlowUped)
        {
            Blow.text = "�����!! ��������� �����(";
        }
        else
            Blow.text = "�����!! ����� �����������)";
        Debug.Log(blow.BlowUped);
    }

    public override void HideState()
    {
        base.HideState();
        button.onClick.RemoveAllListeners();
        backButton.onClick.RemoveAllListeners();
    }

    public void OnButtonClicked()
    {
        OnButtonClick?.Invoke();
    }
    public void OnPreviousButtonClicked()
    {
        OnPreviousButtonClick?.Invoke();
    }
}
